# README

API construida en [Laravel](https://laravel.com/) para consultar los códigos postales de México basado en los datos del [Servicio Postal de México](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx) (Última Actualización de Información: Febrero 14 de 2021).

## Información del desarrollo
Para poder crear el API lo primero que tuve que hacer fue pasar los datos que descargue en formato `.xls` de la pagina oficial del [Servicio Postal de México](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx)  a una base de datos usando [Laravel Excel](https://laravel-excel.com/). 

Una ves importados los datos de los datos procedi a crear un endpoint para la consulta de los códigos postales. 

**Ruta del endpoint**
https://codes-api.lizethaguilar.com/api/zip-codes/{code}

El API esta limitado a 15 peticiones por minuto por IP

Existe otra ruta que utilice para importar los datos referente a los códigos postales y que en producción esta deshabilitada. 

Ruta del archivo

`/public/CPdescarga.xls`

**Ruta para importar información**

https://codes-api.lizethaguilar.com/api/zip-codes/import-data

## Instalación
1. Descargar repositorio `git clone https://carlosagaton@bitbucket.org/carlosagaton/zipcodes-api.git`
2. composer install
3. Crear archivo `.env` basado en el archivo .`env example`
4. Configurar base de datos
5. ¡Listo!
