<?php

namespace App\Imports;

use App\Models\Code;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CodesImport implements ToModel, WithChunkReading, WithBatchInserts, WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        return new Code([
            'd_codigo' => $row['d_codigo'],
            'd_asenta' => $row['d_asenta'],
            'd_tipo_asenta' => $row['d_tipo_asenta'],
            'd_mnpio' => $row['d_mnpio'],
            'd_estado' => $row['d_estado'],
            'd_ciudad' => $row['d_ciudad'],
            'd_cp' => $row['d_cp'],
            'c_estado' => $row['c_estado'],
            'c_oficina' => $row['c_oficina'],
            'c_cp' => $row['c_cp'],
            'c_tipo_asenta' => $row['c_tipo_asenta'],
            'c_mnpio' => $row['c_mnpio'],
            'id_asenta_cpcons' => $row['id_asenta_cpcons'],
            'd_zona' => $row['d_zona'],
            'c_cve_ciudad' => $row['c_cve_ciudad'],
        ]);
        
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
