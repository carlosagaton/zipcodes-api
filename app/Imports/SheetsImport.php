<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SheetsImport implements WithMultipleSheets, WithBatchInserts, WithChunkReading
{

    use Importable;

    public function sheets(): array
    {

        return [ 
            0 => new CodesImport(),
            1 => new CodesImport(),
            2 => new CodesImport(),
            3 => new CodesImport(),
         ];
    }
    

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
