<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function successResponse($data, $message)
    {
        return response()->json($data);
    }

    public function failResponse($data, $message, $code)
    {
        return response()->json(
            [
                'result' => false,
                'message' => $message,
                'errors' => $data,
                'status' => $code
            ], $code
        );
    }
}
