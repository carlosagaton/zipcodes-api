<?php

namespace App\Http\Controllers;

use App\Imports\CodesImport;
use App\Models\Code;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;

class CodesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $code)
    {
        $codes = Code::where('d_codigo', $code)->get();

        if (!$codes->isEmpty()) {
            $response = $this->responseData($codes);

            return response()->json($response);
        }

        return  $this->failResponse(['NotFound'], "No results found for code $code", 404);
    }

    public function responseData($codes)
    {
        $code = $codes->first();
        $response = [
            "zip_code" => $code->d_codigo,
            "locality" => $code->d_ciudad ?? '',
            "federal_entity" => [
                'key' => intval($code->c_estado),
                'name' => strtoupper($code->d_estado),
                'code' => strtoupper($code->d_estado)
            ],
            "settlements" => [],
            "municipality" => [
                'key' => intval($code->c_mnpio),
                'name' => strtoupper($code->d_mnpio)
            ]
        ];

        foreach($codes as $settlement) {
            $settlement = [
               'key' => intval($settlement->id_asenta_cpcons),
               'name' => strtoupper($settlement->d_asenta),
               'zone_type' => $settlement->d_zona,
               'settlement_type' => ['name' => strtoupper($settlement->d_tipo_asenta)]
            ];

            array_push($response['settlements'], $settlement);
        } 

        return $response;
    }
}
