<?php

namespace App\Http\Controllers;

use App\Imports\CodesImport;
use App\Imports\SheetsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExcelController extends Controller
{
    public function import()
    {
        set_time_limit(300);
        return (new CodesImport)->import('CPdescarga.xls', null, \Maatwebsite\Excel\Excel::XLS);
    }
}
